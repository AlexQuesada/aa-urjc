package Alex.Quesada.AA.EliminacionRecursividad;

public class Practica4 {

    public static int f(int x, int y) {
        if (y == 0)
            return x;
        else
            return f(x, y - 1) + f(x + 1, y - 1);
    }

    public static int[][] generateTable(int y) {
        int[][] table = new int[y + 1][];
        for (int i = 0; i <= y; i++) {
            table[i] = new int[y + 1 - i];
            for (int j = 0; j < table[i].length; j++)
                table[i][j] = -1;
        }
        return table;
    }

    public static int memorizacion(int x, int y) {
        int[][] table = generateTable(y);
        auxMemorizacion(x, y, table, x, y);
        return table[0][0];
    }

    public static void auxMemorizacion(int n, int m, int[][] table, int x, int y) {
        int auxX = n >= (x+y) ? y : n-x;
        int auxY = table[auxX].length - 1 - m;
        if (table[auxX][auxY] == -1) {
            if (m == 0)
                table[auxX][auxY] = n;
            else {
                auxMemorizacion(n, m - 1, table, x, y);
                auxMemorizacion(n + 1, m - 1, table, x, y);
                table[auxX][auxY] = table[auxX][auxY+1] + table[auxX+1][auxY];
            }
        }
    }
    public static int tabulacion(int n, int m){
        int[][] table = generateTable(m);
        int aux = n;

        for (int i = 0; i < m+1; i++)
            table[i][table[i].length-1] = aux++;

        for (int i = m-1; i >= 0; i--){
            for (int j = 0; j < i+1; j++){
                table[i-j][j] = table[i-j][j+1] + table[i-j+1][j];
            }
        }
        return table[0][0];
    }


    public static void main(String[] args) {
//        int[][] table = generateTable(3);
//        for(int i[]: table){
//            for(int j: i){
//                System.out.print("x");
//            }
//            System.out.println("");
//        }
        System.out.println(f(3, 8));
        System.out.println(memorizacion(3, 8));
        System.out.println(tabulacion(3, 8));
    }
}
