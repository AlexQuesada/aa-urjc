package Alex.Quesada.AA.Heuristicos.Practica2;

public class Practica2 {

    /**
     * Algoritmo heurístico 1 (enunciado)
     * @param a lista de sueldos alta tension para n semanas
     * @param b lista de sueldos baja tension para n semanas
     * @return sueldo maximal para n semanas
     */
    public static int trabajosH1 (int[] a, int[] b){
        int sol = 0;
        int nSemanas = a.length;
        for (int i  = 0; i+1 < nSemanas; i+=2){
            int beneficioA = a[i+1];
            int beneficioB = b[i] + b[i+1];
            sol += Math.max(beneficioA, beneficioB);
        }
        if (nSemanas%2 != 0)
            sol+= b[nSemanas-1];

        return sol;
    }

    /**
     * Algoritmo heurístico 2:
     * Evaluo tareas de 2 en 2, empezando por el final, y en caso de que el numero de semanas sea impar,
     * en la primera semana coje la tarea que mas beneficio de, porque en la primera semana las tareas de estres
     * son gratis (no necesitan semana de descanso previa)
     *
     * @param a lista de sueldos alta tension para n semanas
     * @param b lista de sueldos baja tension para n semanas
     * @return sueldo maximal para n semanas
     */
    public static int trabajosH2 (int[] a, int[] b){
        int sol = 0;
        int nSemanas = a.length;

        //recorro el algoritmo del final al principio.
        for (int i = nSemanas-1; i > 0; i-=2){
            int beneficioA = a[i];
            int beneficioB = b[i-1] + b[i];
            sol += Math.max(beneficioA, beneficioB);
        }
        if (nSemanas%2 != 0)
            sol+=Math.max(a[0], b[0]);

        return sol;
    }

    public static int trabajosVA(int[] a, int[] b){
        return auxVA(a,b,true,0,0,0);
    }

    private static int auxVA(int[] a, int[] b, boolean rest, int lvl, int solActual, int solMejor){
        if (lvl == a.length) {
            if (solActual > solMejor)
                solMejor = solActual;
        } else {
            for (int i = 0; i < 3; i++) {
                if (i == 0)         //baja tension
                    solMejor = auxVA(a, b, false, lvl + 1, solActual + b[lvl], solMejor);
                else if (i == 1)    //sin tarea
                    solMejor = auxVA(a, b, true, lvl + 1, solActual, solMejor);
                else                //alta tension
                    if (rest)
                        solMejor = auxVA(a, b, false, lvl + 1, solActual + a[lvl], solMejor);
            }
        }
        return solMejor;
    }

    public static int trabajosRP(int[] a, int[] b){
        return auxRP(a,b,true,0,0,0);
    }

    private static int auxRP(int[] a, int[] b, boolean rest, int lvl, int solActual, int solMejor){

        if (lvl == a.length) {
            if (solActual > solMejor)
                solMejor = solActual;
        }
        else if (lvl == a.length-1){
            for (int i = 0; i < 2; i++){
                if (i == 0)
                    solMejor = auxRP(a, b, false, lvl + 1, solActual + b[lvl], solMejor);
                else
                if (rest)
                    solMejor = auxRP(a, b, false, lvl + 1, solActual + a[lvl], solMejor);
            }
        }
        else {
            for (int i = 0; i < 3; i++) {
                if (i == 0) {
                    solMejor = auxRP(a, b, false, lvl + 1, solActual + b[lvl], solMejor);

                } else if (i == 1) {
                    if (!rest || lvl == 0)
                        solMejor = auxRP(a, b, true, lvl + 1, solActual, solMejor);

                } else
                    if (rest)
                        solMejor = auxRP(a, b, false, lvl + 1, solActual + a[lvl], solMejor);
            }
        }
        return solMejor;
    }

    public static void main(String args[]){
        int a[] = {13,20,10};
        int b[] = {6,6,10};

        System.out.println(trabajosRP(a,b));
        System.out.println(trabajosVA(a,b));
    }
}
