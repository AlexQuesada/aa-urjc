package Alex.Quesada.AA.Voraces;

public class SelecActividades {

    /**
     * actividades ordenadas en orden creciente de fin
     * @param comienzo lista de horas de comienzo
     * @param fin lista de horas de fin
     * @return numero maximo de actividades
     */
    public static int selecActividades1 (int[] comienzo, int[] fin) {
        int num = 1;
        int i = 0;

        for (int j=1; j<comienzo.length; j++) {
            if (comienzo[j]>=fin[i]) {
                num++;
                i = j;
            }
        }

        return num;
    }

    /**
     * imprime por pantalla cuales son las actividades de la solución
     * actividades ordenadas en orden creciente de fin
     * @param comienzo lista de horas de comienzo
     * @param fin lista de horas de fin
     * @return numero maximo de actividades simultaneas
     */
    public static int selecActividades2 (int[] comienzo, int[] fin) {
        boolean[] sol = new boolean[comienzo.length];
        sol[0] = true;
        int num = 1;
        int i = 0;

        for (int j=1; j<comienzo.length; j++) {
            if (comienzo[j]>=fin[i]) {
                sol[j] = true;
                num++;
                i = j;
            }
            else
                sol[j] = false;
        }

        imprimir (sol);
        return num;
    }

    /**
     * imprime por pantalla
     * @param sol lista de booleanos que representa una solucion
     */
    private static void imprimir (boolean[] sol) {
        for (boolean b : sol) System.out.print(b + "  ");
        System.out.println ();
    }
}
