package Alex.Quesada.AA.Voraces;

public class CaminosMinimos {

    public static int[] Dijkstra1 (int[][] grafo, int origen) {
        // calcula las longitudes de los caminos mínimos
        // conjunto de candidatos, con todos los nodos salvo el origen
        boolean[] candidatos = new boolean[grafo.length];
        for (int i=0; i<candidatos.length; i++)
            candidatos[i] = true;
        candidatos[origen] = false;
        // distancias inicializadas como vienen dadas en el grafo
        int[] distancias = new int[grafo.length];
        for (int i=0; i<distancias.length; i++)
            distancias[i] = grafo[origen][i];
        distancias[origen] = 0;
        // iteración para todos los candidatos
        for (int i=0; i<grafo.length-1; i++) {
            // se busca un primer candidato
            int j;
            for (j=0; j<candidatos.length && !candidatos[j]; j++) {}
            int menor = j;
            // se busca el candidato más cercano al origen
            for (; j<candidatos.length; j++) {
                if (candidatos[j] && distancias[j]<distancias[menor])
                    menor = j;
            }
            candidatos[menor] = false;
            // se actualizan las distancias mínimas que pasan por el nuevo candidato
            for (j=0; j<candidatos.length; j++) {
                if (candidatos[j])
                    distancias[j] = Math.min (distancias[j], distancias[menor]+grafo[menor][j]);
            }
        }
        return distancias;
    }

    public static int[] Dijkstra2 (int[][] grafo, int origen) {
        // calcula las longitudes de los caminos mínimos y los propios caminos mínimos
        // conjunto de candidatos, con todos los nodos salvo el origen
        boolean[] candidatos = new boolean[grafo.length];
        for (int i=0; i<candidatos.length; i++)
            candidatos[i] = true;
        candidatos[origen] = false;
        // distancias inicializadas como vienen dadas en el grafo, y predecesores
        int[] distancias = new int[grafo.length];
        int[] predecesores = new int[grafo.length];
        for (int i=0; i<distancias.length; i++) {
            distancias[i] = grafo[origen][i];
            predecesores[i] = grafo[origen][i]<1000?origen:0; /*valor tomado como infinito*/
        }
        distancias[origen] = 0;
        predecesores[origen] = 0;
        // iteración para cada candidato
        for (int i=0; i<grafo.length-1; i++) {
            // se busca un primer candidato
            int j;
            for (j=0; (j<candidatos.length) && !(candidatos[j]); j++) {}
            int menor = j;
            // se busca el candidato más cercano al origen
            for (; j<candidatos.length; j++) {
                if ((candidatos[j]) && (distancias[j]<distancias[menor]))
                    menor = j;
            }
            candidatos[menor] = false;
            // se actualizan las distancias mínimas que pasan por el nuevo candidato
            for (j=0; j<candidatos.length; j++) {
                if (candidatos[j])
                    if (distancias[menor]+grafo[menor][j]<distancias[j]) {
                        distancias[j] = distancias[menor]+grafo[menor][j];
                        predecesores[j] = menor;
                    }
            }
        }
        imprimirCaminos (predecesores, origen, distancias);
        return distancias;
    }

    private static void imprimirCaminos (int[] preds, int origen, int[] distancias) {
        for (int i=0; i<preds.length; i++) {
            System.out.print ("Nodo "+i+" : camino = ");
            if (distancias[i]==1000) /*valor tomado como infinito*/
                System.out.print ("-");
            else
                imprimirCamino(preds,origen,i);
            System.out.println ();
        }
    }

    private static void imprimirCamino (int[] preds, int origen, int j) {
        if (j!=origen)
            imprimirCamino (preds,origen,preds[j]);
        System.out.print (j+" ");
    }
}
