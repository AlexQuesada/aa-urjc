package Alex.Quesada.AA.Voraces;

public class SecTareasConPlazos{


    public static int SecTareasConPlazos1 (int[] benefinicios, int[] plazos) {
        // tareas ordenadas según beneficios decrecientes
        int[] tareas = new int[benefinicios.length];
        tareas[0] = 0;
        int j = 0;
        int b = benefinicios[0];
        for (int i=1; i<benefinicios.length; i++) {
            int k = j;
            while (k>=0 && plazos[tareas[k]]>plazos[i] && plazos[tareas[k]]>k)
                k--;
            if (k<0 || (plazos[tareas[k]]<=plazos[i] && plazos[i]>k)) {
                for (int l=j; l>=k+1; l--)
                    tareas[l+1] = tareas[l];
                tareas[k+1] = i;
                b += benefinicios[i];
                j++;
            }
        }
        imprimir (tareas, j);
        return b;
    }

    public static int SecTareasConPlazos2 (int[] beneficios, int[] plazos) {
        // tareas sin ordenar
        // se ordenan los índices según beneficios decrecientes
        int[] is = new int[beneficios.length];
        is = ordenarIndices(beneficios);
        // el algoritmo voraz, usando el vector de índices 'is'
        int[] tareas = new int[beneficios.length];
        tareas[0] = is[0];
        int j = 0;
        int b = beneficios[is[0]];
        for (int i=1; i<beneficios.length; i++) {
            int k = j;
            while (k>=0 && plazos[tareas[k]]>plazos[is[i]] && plazos[tareas[k]]>k)
                k--;
            if (k<0 || (plazos[tareas[k]]<=plazos[is[i]] && plazos[is[i]]>k)) {
                for (int l=j; l>=k+1; l--)
                    tareas[l+1] = tareas[l];
                tareas[k+1] = is[i];
                b += beneficios[is[i]];
                j++;
            }
        }
        imprimir (tareas, j);
        return b;
    }

    private static int[] ordenarIndices (int[] v1) {
        int[] v2 = new int[v1.length];
        v2[0] = 0;
        for (int i=1; i<v1.length; i++) {
            int aux = v1[i];
            int j;
            for (j=i-1; j>=0 && v1[v2[j]]<aux; j--)
                v2[j+1] = v2[j];
            v2[j+1] = i;
        }
        return v2;
    }

    private static void imprimir (int[] v, int n) {
        for (int i=0; i<=n; i++)
            System.out.print ((v[i]<10?("  "+v[i]):(v[i]<100?" "+v[i]:v[i]))+" ");
        System.out.println();
    }

}