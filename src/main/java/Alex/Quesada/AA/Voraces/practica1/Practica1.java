package Alex.Quesada.AA.Voraces.practica1;

public class Practica1 {

    /**
     * Algoritmo voraz idealizado
     * @param ts array con lista de tareas ordenadas por orden de duración
     * @param n numero de procesadores
     * @return tiempo total de espera de las tareas
     */
    public static int tareasIdeal (int[] ts, int n){
        int tEsperaTotal= 0; //tiempo de espera total

        //array que contiene los tiempos de espera parciales de todas las tareas
        int[] tEsperaTareas = new int[ts.length];

        //calculo los tiempos de espera parciales
        for (int i = 0; i < ts.length; i++)
            tEsperaTareas[i] = (i < n) ? ts[i] : ts[i] + tEsperaTareas[i - n];

        //sumo los tiempos de espera parciales
        for (int value : tEsperaTareas)
            tEsperaTotal += value;

        return tEsperaTotal;
    }

    /**
     * Algoritmo voraz real
     * @param ts array con lista de tareas no ordenadas
     * @param n numero de procesadores
     * @return tiempo total de espera de las tareas
     */
    public static int tareasReal (int[] ts, int n){
        int tEsperaTotal= 0; //tiempo de espera total

        //ordeno la lista de tareas, guardando el orden de los indices del array de tareas
        int [] indices = Sort.quickSort(ts);

        //array que contiene los tiempos de espera parciales de todas las tareas
        int[] tEsperaTareas = new int[ts.length];

        //calculo los tiempos de espera parciales
        for (int i = 0; i < ts.length; i++)
            tEsperaTareas[i] = (i < n) ? ts[indices[i]] : ts[indices[i]] + tEsperaTareas[i - n];

        //sumo los tiempos de espera parciales
        for (int value : tEsperaTareas)
            tEsperaTotal += value;

        return tEsperaTotal;
    }

}
