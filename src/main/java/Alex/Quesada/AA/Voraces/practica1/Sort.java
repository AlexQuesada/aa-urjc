package Alex.Quesada.AA.Voraces.practica1;

public class Sort {

    /**
     * Algortimo burbuja para indices de un array
     */
    public static int[] bubbleSort(int[] ts) {
        int n = ts.length;
        int[] ind = arrayIndices(n);

        for (int i = 0; i <= n - 2; i++)
            for (int j = n - 1; j > i; j--)
                if (ts[ind[j-1]] > ts[ind[j]]) {
                    int aux = ind[j];
                    ind[j] = ind[j-1];
                    ind[j-1] = aux;
                }

        return ind;
    }

    /**
     * Algortimo quicksort para indices de un array
     */
    public static int[] quickSort(int[] ts){
        int[] ind = arrayIndices(ts.length);
        quickSortAux(ts, ind,0, ts.length-1);
        return ind;
    }

    private static void quickSortAux(int[] ts, int[] ind, int beg, int end){
        if (beg < end){
            int part = particion(ts,ind,beg,end);
            quickSortAux(ts, ind, beg, part-1);
            quickSortAux(ts, ind, part + 1, end);
        }
    }
    private static int particion(int[] ts, int[] ind, int beg, int end) {
        int pivote = ts[ind[end]];
        int i = (beg - 1);

        for (int j = beg; j < end; j++)
            if (ts[ind[j]] <= pivote) {
                i++;
                int aux = ind[i];
                ind[i] = ind[j];
                ind[j] = aux;
            }

        int aux = ind[i+1];
        ind[i+1] = ind[end];
        ind[end] = aux;

        return i+1;
    }

    private static int[] arrayIndices(int n){
        int[] indices = new int[n];
        for (int i = 0; i < n; i++)
            indices[i]= i;
        return indices;
    }
}
