package Alex.Quesada.AA.Practica1;

import Alex.Quesada.AA.Voraces.practica1.Practica1;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

@RunWith(Parameterized.class)
public class AlgortimoRealTest {


    // Casos de prueba, {ts, n, resultado}
    @Parameters (name = "Test nº {index}")
    public static Collection data() {

        int[][][] values = {

                //Con valores ordenados
                {{1, 2, 3, 4, 5}, {2}, {22}}, //ejemplo enunciado práctica
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {2}, {125}},
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {3}, {94}},
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {4}, {79}},
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {5}, {70}},
                {{2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, {2}, {250}},
                {{2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, {3}, {188}},
                {{2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, {4}, {158}},
                {{2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, {5}, {140}},

                //con valores desordenados
                {{5, 3, 1, 2, 4}, {2}, {22}}, //ejemplo enunciado práctica desordenado
                {{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, {2}, {125}},
                {{1, 2, 3, 4, 5, 10, 9, 8, 7, 6}, {3}, {94}},
                {{1, 2, 10, 9, 3, 4, 8, 7, 5, 6}, {4}, {79}},
                {{9, 7, 1, 10, 8, 4, 2, 5, 6, 3}, {5}, {70}},
                {{20, 18, 16, 14, 12, 10, 8, 6, 4, 2}, {2}, {250}},
                {{20, 18, 4, 6, 10, 14, 16, 8, 2, 12}, {3}, {188}},
                {{14, 16, 20, 2, 6, 8, 18, 12, 4, 10}, {4}, {158}},
                {{14, 20, 6, 2, 18, 4, 10, 8, 12, 16}, {5}, {140}}
        };
        return Arrays.asList(values);
    }

    @Parameter(0) public int[] ts;
    @Parameter(1) public int[] n;
    @Parameter(2) public int[] sol;

    @Test
    public void test(){
        assertThat(Practica1.tareasReal(ts, n[0])).isEqualTo(sol[0]);
    }
}
