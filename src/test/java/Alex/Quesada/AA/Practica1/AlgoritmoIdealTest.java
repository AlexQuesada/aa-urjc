package Alex.Quesada.AA.Practica1;

import Alex.Quesada.AA.Voraces.practica1.Practica1;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.*;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

@RunWith(Parameterized.class)
public class AlgoritmoIdealTest {

    // Casos de prueba, {ts, n, resultado}
    @Parameters (name = "Test nº {index}")
    public static Collection data() {

        int[][][] values = {

                {{1, 2, 3, 4, 5}, {2}, {22}}, //ejemplo enunciado práctica
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {2}, {125}},
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {3}, {94}},
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {4}, {79}},
                {{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {5}, {70}},
                {{2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, {2}, {250}},
                {{2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, {3}, {188}},
                {{2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, {4}, {158}},
                {{2, 4, 6, 8, 10, 12, 14, 16, 18, 20}, {5}, {140}}
        };
        return Arrays.asList(values);
    }

    @Parameter(0) public int[] ts;
    @Parameter(1) public int[] n;
    @Parameter(2) public int[] sol;

    @Test
    public void test(){
        assertThat(Practica1.tareasIdeal(ts, n[0])).isEqualTo(sol[0]);
    }
}
